# Puzzle15

A simple browser game to get some introduction with Typescript and Angular.

It's possible to move tiles by clicking on them with mouse or by pressing arrow keys.

## Demo

I deployed the application to bitbucket pages. Check it [here](https://gta2k.bitbucket.io/Puzzle15/)