import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  ComponentFactoryResolver,
  HostListener,
  ViewChild,
  ViewContainerRef
} from '@angular/core';
import {TileComponent} from './tile/tile.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements AfterViewInit {
  @ViewChild('container', {read: ViewContainerRef})
  container: ViewContainerRef;
  tiles: TileComponent[] = [];
  isWin = false;

  constructor(
    private componentFactoryResolver: ComponentFactoryResolver,
    private changeDetectorRef: ChangeDetectorRef) {
  }

  ngAfterViewInit(): void {
    const factory = this.componentFactoryResolver.resolveComponentFactory(TileComponent);

    for (let id = 0; id < 16; id++) {
      const componentRef = this.container.createComponent(factory);
      componentRef.instance.init(id, this);
      this.tiles.push(componentRef.instance);
    }

    this.changeDetectorRef.detectChanges();
  }

  @HostListener('window:keyup', ['$event'])
  keyEvent(event: KeyboardEvent): void {
    switch (event.code) {
      case 'ArrowRight':
      case 'ArrowLeft':
      case 'ArrowUp':
      case 'ArrowDown':
        this.moveByArrows(event.code);
        break;
    }
  }

  shuffleTiles(): void {
    for (let i = this.tiles.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [this.tiles[i].id, this.tiles[j].id] = [this.tiles[j].id, this.tiles[i].id];
    }
  }

  tileAtPosition(x: number, y: number): TileComponent {
    return  this.tiles.find(e => e.positionX === x && e.positionY === y);
  }

  move(tile: TileComponent): void {
    const x = tile.positionX;
    const y = tile.positionY;

    if (x < 4 && this.tileAtPosition(x + 1, y).isEmpty()) {
      this.tileAtPosition(x + 1, y).positionX--;
      tile.positionX++;
    }
    if (x > 1 && this.tileAtPosition(x - 1, y).isEmpty()) {
      this.tileAtPosition(x - 1, y).positionX++;
      tile.positionX--;
    }

    if (y < 4 && this.tileAtPosition(x, y + 1).isEmpty()) {
      this.tileAtPosition(x, y + 1).positionY--;
      tile.positionY++;
    }
    if (y > 1 && this.tileAtPosition(x, y - 1).isEmpty()) {
      this.tileAtPosition(x, y - 1).positionY++;
      tile.positionY--;
    }

    this.checkWin();
  }

  private moveByArrows(key: string): void {
    const emptyTile = this.tiles.find(e => e.id === 0);

    switch (key) {
      case 'ArrowRight':
        if (emptyTile.positionX > 1) {
          this.tileAtPosition(emptyTile.positionX - 1, emptyTile.positionY).positionX++;
          emptyTile.positionX--;
        }
        break;

      case 'ArrowLeft':
        if (emptyTile.positionX < 4) {
          this.tileAtPosition(emptyTile.positionX + 1, emptyTile.positionY).positionX--;
          emptyTile.positionX++;
        }
        break;

      case 'ArrowUp':
        if (emptyTile.positionY < 4) {
          this.tileAtPosition(emptyTile.positionX, emptyTile.positionY + 1).positionY--;
          emptyTile.positionY++;
        }
        break;

      case 'ArrowDown':
        if (emptyTile.positionY > 1) {
          this.tileAtPosition(emptyTile.positionX, emptyTile.positionY - 1).positionY++;
          emptyTile.positionY--;
        }
        break;
    }

    this.checkWin();
  }

  private checkWin(): void {
    for (let id = 1; id < 16; id++) {
      const tile = this.tiles.find(t => t.id === id);

      const expectedX = id % 4 ? id % 4 : 4;
      const expectedY = Math.ceil(tile.id / 4);

      if (tile.positionX === expectedX && tile.positionY === expectedY) {
        continue;
      } else {
        this.isWin = false;
        return;
      }
    }

    this.isWin = true;
  }
}
