import {Component} from '@angular/core';
import {AppComponent} from '../app.component';

@Component({
  selector: 'app-tile',
  templateUrl: './tile.component.html',
  styleUrls: ['./tile.component.css']
})
export class TileComponent {

  id: number;
  parentRef: AppComponent;

  // coordinates
  positionX: number;
  positionY: number;

  init(id: number, parentRef: AppComponent): void {
    this.parentRef = parentRef;
    this.id = id;

    if (id === 0) {
      this.positionX = 4;
      this.positionY = 4;
      return;
    }

    this.positionX = this.id % 4;
    if (this.positionX === 0) {
      this.positionX = 4;
    }
    this.positionY = Math.ceil(this.id / 4);
  }

  onClick(): void {
    this.parentRef.move(this);
  }

  isEmpty(): boolean {
    return this.id === 0;
  }

}
